# Easy posts getter for r/spsememes

[NPM page](https://www.npmjs.com/package/spse-memes)  


#### Install

`npm i spse-memes` or `yarn add spse-memes`


#### Usage Examples

useMemes() hook - Render Meme images
```javascript
import {useMemes} from "spse-memes";
const MemesList = () => {
    const memes = useMemes();
    return <div>
        memes.map(meme => <img src={meme.image}/>)
    </div>
}
```

getMemes() function - Print meme objects to console 
```javascript
import {getMemes} from "spse-memes";
const printMemesObjects = () => {
    getMemes().then(memes => console.log(memes));
}
```
