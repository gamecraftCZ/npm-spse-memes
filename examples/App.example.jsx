import React from 'react';
import {useMemes} from "spse-memes";

function Meme({meme}) {
    return <div style={{width: "400px", border: "1px solid black"}}>
        <h2>title: {meme.title}</h2> <br/>
        <h3>author: {meme.author}</h3> <br/>
        <img src={meme.image} style={{width: "400px"}} alt={meme.title}/>
    </div>
}

function App() {
    const memes = useMemes();

    return (
        <>
            There are {memes.length} memes loaded.
            <div style={{display: "flex", flexWrap: "wrap"}}>
                {memes.map(meme => <Meme meme={meme} />)}
            </div>
        </>
    );
}

export default App;
