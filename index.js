const {useEffect, useState} = require("react");
const axios = require("axios");

const redditUrl = "https://www.reddit.com/r/spseplzen/hot.json?limit=100";

class Meme {
    constructor(url, title, text, image, author) {
        this.url = url;
        this.title = title;
        this.image = image;
        this.author = author;
    }

    static fromRedditApi(obj) {
        const meme = new Meme();
        meme.url = `https://reddit.com${obj.data.permalink}`;
        meme.title = obj.data.title;

        meme.image = obj.data.url;
        if (meme.image) {
            if (! meme.image.includes("https://i.redd.it/")) {
                meme.image = this.getUrlFrom(obj, undefined);
            }
        } else {
            meme.image = this.getUrlFrom(obj, undefined);
        }


        meme.author = obj.data.author;
        return meme;
    }

    static getUrlFrom(obj, fallbackValue) {
        if (obj.data.preview && obj.data.preview.images) {
            const imagesCount = obj.data.preview.images.length;
            if (imagesCount > 0) {
                const imageObj = obj.data.preview.images[imagesCount - 1];
                return imageObj.resolutions[imageObj.resolutions.length - 1].url.replace(/&amp;/g,"&");
            }
        }
        return fallbackValue;
    }
}

const getMemes = async () => {
    return axios.get(redditUrl)
        .then(res => res.data.data.children)
        .then(memes => memes.map(
            meme => Meme.fromRedditApi(meme)
        ))
        .then(memes => memes.filter(
            meme => meme.image
        ));
};

const useMemes = () => {
    const [memes, setMemes] = useState([]);

    useEffect(() => {
        getMemes().then(mem => setMemes(mem))
    }, []);

    return memes;
};

module.exports = {Meme, getMemes, useMemes};
